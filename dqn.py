import torch 
import torch.nn as nn 
import torch.nn.functional as F
from torch.nn.modules.activation import ReLU
from torch.nn.modules.linear import Linear
import torch.optim as optim
import numpy as np 

class DQN(nn.Module):
   def __init__(self, input, output):
        super(DQN, self).__init__()
        self.fc = nn.Sequential(
           nn.Linear(input, 500),
           nn.ReLU(),
           nn.Linear(500, 400),
           nn.ReLU(),
           nn.Linear(400, 300),
           nn.ReLU(),
           nn.Linear(300, 200),
           nn.ReLU(),
           nn.Linear(200, 100),
           nn.ReLU(),
           nn.Linear(100, output)
        )

   def forward(self, x):
      return self.fc(x)
   
   def backPropagate(self, x):
      pass

