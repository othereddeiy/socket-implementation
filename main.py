from logging import fatal
from tkinter.constants import TRUE
import q_agent_inference
from q_agent import q_agent
from util import visualizer, colours
import json
#Lagerrahmenbedigungen
'''
Die Werte hier müssen mit denen in Q-Agent.py und denen im Modell übereinstimmen 
'''
ACTION_ROWS = 10
ACTION_COLUMNS = 5
ACTION_LEVELS = 3
ACTION_OPTIONS = ACTION_COLUMNS*ACTION_ROWS*ACTION_LEVELS
CONTAINER_ATTRIBUTES = 4
visualizer = visualizer(ACTION_ROWS, ACTION_COLUMNS, ACTION_LEVELS)
colours = colours()
mode = "RL" # Kann "RL" oder "Random" sein
train = True
if train:
    RMGserver = q_agent(5050, 64, 'utf-8',"gpu", (ACTION_OPTIONS*CONTAINER_ATTRIBUTES+5), ACTION_COLUMNS*ACTION_ROWS, "RL")
    RMGserver.start()
    RMGserver.close()
else:
    RMGserver = q_agent_inference.q_agent_inference(5050, 64, 'utf-8',"gpu", (ACTION_OPTIONS*CONTAINER_ATTRIBUTES+5), ACTION_COLUMNS*ACTION_ROWS, mode, record=True)
    RMGserver.start()
    RMGserver.close()
    try:
        storage_step = int(input(colours.purplify("Please enter the step you want to see visualized as a plot! Anything else shuts the programme down.")))
        while type(storage_step) == int or type(storage_step) == float:
                with open(f"{q_agent_inference.REV_PATH}/req{storage_step}.json") as observation_file:
                    observation = json.load(observation_file)
                    observation_file.close()
                    visualizer.read_in_container_values(observation)
                    visualizer.visualizeStorage()
                    storage_step = int(input(colours.purplify("Please enter the step you want to see visualized as a plot! Anything that is not an integer or unavailable as a step leads to a shutdown.")))
            
    except:
        print(colours.greenify("PLOT CLOSED"))
