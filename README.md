# Socket-Implementation 
Socket-Implementierung eines Reinforcement Learning Modells, das mit einem Plant Simulation Model zusammenarbeitet :D

## Abhängigkeiten:
Python, Pytorch, Tensorboard, Plant Simulation, Numpy

Python Download:
https://www.python.org/downloads/ 

Pytorch Einrichtung: 
Für Pytorch muss auf https://pytorch.org/ das OS, das Package (ich hab pip genutzt) und im Falle dessen, dass (k)eine Nvidia GPU installiert ist, der richtige Command Line Befehl ins Terminal kopiert werden und das CUDA-Toolkit auf https://developer.nvidia.com/cuda-downloads gedownloadet werden.

Tensorboard:
`pip install tensorboard` in die Kommandozeile nach der Python Installation einfügen, eventuell ist Tensorboard auch mit der Pytorch Installation inkludiert. 

## Einrichtung
### Voraussetzungen:
- Falls keine CUDA fähige GPU installiert ist, außerdem in main.py `gpu` durch `cpu` verbessern.
- Mit dem TU-VPN verbinden

### Training
1. Im plant sim-Ordner befindet sich das plant simulation file mit dem Namen 'Simulationsmodell.spp'. Dieses öffnen.

2. Mit dem Windows Terminal z. B. in das Verzeichnis bewegen in welchem sich das lokale Repository oder der Download befindet, falls nicht aus der Entwicklungsumgebung heraus geöffnet, und `python main.py` in die Kommandozeile einfügen. Daraufhin die angezeigte IP-Adresse kopieren.

3. Die kopierte IP-Adresse in der gleichnamigen globalen Variable in `MeinNetzwerk` in der Plant Simulation Datei einfügen und das Simulationsmodell mit Auswahl `Reinforcement Learning` im Dropdown-Menü aus `MeinNetzwerk` heraus starten.

### Inferenz 
Die selben Schritte wie für das Training.
Aber: In main.py das flag `train` auf `false` setzen.



