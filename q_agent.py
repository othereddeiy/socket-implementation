import json
from typing import Counter
from numpy import random
import torch
import collections
import numpy as np
import time
import random
import logging
from torch.nn.modules import loss 
import torch.optim as optim
import torch.nn as nn
from datetime import datetime
#Workspace
from dqn import DQN
from util import colours, fileDate, fileManager, visualizer
from experienceBuffer import ExperienceBuffer
from server import server
#Tensorboard
from torch.utils.tensorboard import SummaryWriter

#Hyperparameter
GAMMA = 0.65
BATCH_SIZE = 32
REPLAY_SIZE = 10000
LEARNING_RATE = 0.15
SYNC_TARGET_REQUESTS = 1000
REPLAY_START_SIZE = 5000
SAVE_MODEL_EVERY = 10000

EPSILON_START = 1.0
EPSILON_FINAL = 0.02
EPSILON_REDUCE_AT = 100000
#Lagerrahmenbedingungen
ACTION_ROWS = 10
ACTION_COLUMNS = 5
ACTION_LEVELS = 3
CONTAINER_ATTRIBUTES = 3

#Unsere Utilities
colours = colours()
fileDate = fileDate()
fileManager = fileManager()
visualizer = visualizer(ACTION_ROWS, ACTION_COLUMNS, ACTION_LEVELS)
#Pfad zum Speichern der Modelle für inference
PATH = f"savedmodels/MODEL_GAMMA_{GAMMA}_LR_{LEARNING_RATE}_{fileDate.getNow()}.pth"
PATH_SAVE = f"savedmodels/MODEL_GAMMA_{GAMMA}_LR_{LEARNING_RATE}_{fileDate.getNow()}"
Model_Counter = 0
#Experience Tuple welches dem ReplayBuffer hinzugefügt wird 
Experience = collections.namedtuple('Experience', field_names=['state', 'action', 'reward', 'done', 'new_state'])

#Unser Tensorboard
writer = SummaryWriter(comment=f" GAMMA={GAMMA} LR={LEARNING_RATE}")
writer_folder_name = fileManager.getLogFolderName(GAMMA, LEARNING_RATE)

class q_agent(server):

   def __init__(self, PORT, HEADER, FORMAT, device, net_input, net_output, mode, exp_buffer_capacity=REPLAY_SIZE):
      super(q_agent, self).__init__(PORT, HEADER, FORMAT)
      self.mode = mode
      self.device = "cuda" if device == "gpu" else "cpu"
      self.last_observation = None
      self.last_state = np.zeros(net_input)
      self.last_action = 0
      self.net = DQN(net_input, net_output).to(self.device)
      self.target_net = DQN(net_input, net_output).to(self.device)
      self.exp_buffer = ExperienceBuffer(exp_buffer_capacity)
      self.REQUEST_MESSAGE = "Request"
      self.OBSERVATION_MESSAGE = "{"
      self.UPDATE_MESSAGE = "Update"
      self.total_Reward = 0
      self.DISCONNECT_MESSAGE = "Disconnect"
      self.Model_Counter = 0

   def receiveRequest(self, conn, addr):
      '''
      Funktion, in welcher das Training stattfindet, wird in der Funktion .start(), des Parents server als thread submitted
      '''
      
      optimizer = optim.Adam(self.net.parameters(), lr = LEARNING_RATE)
      time_list = []
      total_rewards = []
      total_loss = []
      best_mean_reward = None
      request_counter = 1
      last_time = 0.0
      print(f"{colours.yellowfy('[NEW CONNECTION]')} {addr} connected")
      connected = True
      while connected:
         
         try:

            #Unser Epsilon EPSILON_START (Anfangs=1 wird linear um 0.01 reduziert, kann aber nie kleiner werden als 0.02, da wir max() anwenden)
            epsilon = max(EPSILON_FINAL, EPSILON_START - request_counter / EPSILON_REDUCE_AT) if self.mode == "RL" else EPSILON_START
            msg_length = conn.recv(self.HEADER).decode(self.FORMAT)
            if msg_length: 
               msg_length = int(msg_length)
               msg = conn.recv(msg_length).decode(self.FORMAT)


               if msg == self.DISCONNECT_MESSAGE:
                  writer.close()
                  print(colours.yellowfy("[EVENT]"), "Disconnect Message was received!")
                  
                  #Nutzer fragen, ob das Modell gespeichert werden soll
                  print(colours.purplify("Save this model? [y/n(any key)]"))
                  response_model = str(input())
                  if response_model == "y" or response_model == "Y":
                     torch.save(self.net.state_dict(), PATH)
                     print(colours.yellowfy("[EVENT]"), colours.greenify("Model was saved!"))
                  else:
                     print(colours.yellowfy("[EVENT]"), colours.redify("Model was not saved!"))
                  #Nutzer fragen, ob die Tensorboard Aufzeichung gespeichert werden soll.
                  print(colours.purplify("Save this tensorboard-logfile? [y/n(any key)]"))
                  response_tensorboard = str(input())
                  if response_tensorboard == "y" or response_tensorboard == "Y":
                     print(colours.yellowfy("[EVENT]"), colours.greenify("Tensorboard logfile was saved!"))
                  else:
                     fileManager.deleteFolder(writer_folder_name)
                     print(colours.yellowfy("[EVENT]"), colours.redify("Tensorboard logfile was not saved!"))
                  connected = False
                  
                  print(colours.yellowfy("[EVENT]"), f"[{addr}] : disconnected")

                  
               elif msg[0:6] == self.UPDATE_MESSAGE:
                  self.last_observation = self.convertObservation(msg[6:])


               elif msg == self.REQUEST_MESSAGE:
                  print("------------------------------------------------------------------------------------------------------------------------------------------------")
                  #Zeit messen
                  start_time = time.time()
                  delta_time = start_time-last_time
                  time_list.append(delta_time)
                  last_time = start_time
                  m_time = np.mean(time_list[100:])
                  print(f"{colours.cyanify('REQUEST')} {request_counter}")
                  request_counter += 1
                  actionSuggestion = self.take_action(epsilon, device=self.device)
                  print(f"EPSILON = {epsilon}")
                  print(f"∆: {delta_time}")
                  writer.add_scalar("Performance/Seconds per Request", m_time, global_step=request_counter)
                  writer.add_scalar("Performance/Requests per Second", 1/m_time, global_step=request_counter)
                  conn.send(actionSuggestion.encode(self.FORMAT))


               if msg[0:1] == self.OBSERVATION_MESSAGE:
                  #Beobachtungswerte für einfacheren Zugriff in einfachen Variablen speichern 
                  observation = self.convertObservation(msg)
                  self.last_observation = observation
                  new_state = self.getBatched_State(observation)
                  reward = self.getReward(observation)
                  actions = self.getActionSpace(observation)
                  done = self.getIsDone(observation)
                  
                  
                  self.total_Reward += reward

                  exp = Experience(self.last_state, self.last_action, reward, done, new_state)
                  #Experience Tuple dem Replay / Experience Buffer hinzufügen
                  self.exp_buffer.append(exp)
                  #Letzten State vorspeichern 
                  self.last_state = new_state
                  #Rewards speichern
                  total_rewards.append(reward)

                  #Mittleren Reward der letzten 100 Steps speichern
                  m_reward = np.mean(total_rewards[100:])

                  if done:
                     #writer.close()
                     #torch.save(self.net.state_dict(), PATH)
                     print(f"{colours.yellowfy('[EVENT]')} {colours.greenify(' Simulation beendet')}")

                  #Plotting
                  writer.add_scalar("Epsilon", epsilon, global_step=request_counter)
                  writer.add_scalar("Reward/Noisy Reward", reward, global_step=request_counter)
                  writer.add_scalar("Reward/Total Reward", self.total_Reward, global_step=request_counter)
                  writer.add_scalar("Reward/Mean Reward (Last 100 steps)", m_reward, global_step=request_counter)


                  #Debugging
                  #print(f"action: {actions}")
                  print(f"REWARD: {reward}")
                  print(f"DONE: {done}")

                  #Speichern
                  if request_counter % SAVE_MODEL_EVERY == 0 or request_counter == 10:
                     torch.save(self.net.state_dict(), f"{PATH_SAVE}_{self.Model_Counter}.pth")
                     self.Model_Counter += 1
                     

                  #Falls die Anzahl an Beobachtungen nicht der Anzahl an Beobachtungen entspricht nach welcher wir unser Netz synchronisieren und backpropagation durchführen 
                  if len(self.exp_buffer) < REPLAY_START_SIZE:
                     conn.send(self.get_confirmation().encode(self.FORMAT))
                     print("------------------------------------------------------------------------------------------------------------------------------------------------")
                     continue
                  
                  #Das Zielnetz und das tatsächliche Netz synchronisieren 
                  if request_counter % SYNC_TARGET_REQUESTS == 0:
                     print(colours.yellowfy("[EVENT]"), " Synchronized")
                     self.target_net.load_state_dict(self.net.state_dict())

                  #Backprop
                  optimizer.zero_grad()
                  batch = self.exp_buffer.sample(BATCH_SIZE)
                  loss_t = calc_loss(batch, self.net, self.target_net, device=self.device)
                  total_loss.append(loss_t.item())
                  m_loss = np.mean(total_loss[100:])
                  writer.add_scalar("Loss/Noisy Loss", loss_t, global_step=request_counter)
                  writer.add_scalar("Loss/Mean Loss", m_loss, global_step=request_counter)
                  loss_t.backward()
                  optimizer.step()
                  conn.send(self.get_confirmation().encode(self.FORMAT))
         except Exception as e:
            torch.save(self.net.state_dict(), PATH)
            writer.close()
            logging.exception('')
            conn.send(self.get_exception().encode(self.FORMAT))
            print(colours.redify("Please reset the simualation to save both the logfile and model!"))

      #Training beendet
      print(colours.greenify("[EVENT] TRAINING ENDED"))
      #Verbindung zum Client beenden
      conn.close()
      #Thread schließen
      return False
      

   def translateAction(self, action, ROWS=ACTION_ROWS, COLUMNS=ACTION_COLUMNS):
      ''' 
      Erhält einen Wert von 0-50 und returned einen (JSON-)String bestehend aus Reihen und Spaltenwerten
      '''
      action += 1
      counter = 1
      for i in range(1, ROWS+1):
         for j in range(1, COLUMNS+1):
            if counter == action:
               rc = { "Header": "Action", "row": i, "column": j}
               rcstr = json.dumps(rc)
               return rcstr
            counter += 1

   def translateActionBack(self, row, column, ROWS=ACTION_ROWS, COLUMNS=ACTION_COLUMNS):
      '''
      Erhält zwei Integer-Werte, die Reihe und Spalte repräsentieren und returned einen Wert von 0-50 analog zu translateAction()
      Hard coded 
      '''
      counter = 1
      for i in range(1, ROWS+1):
         for j in range(1, COLUMNS+1):
            if row==i and column==j:
               return counter-1
            counter += 1

   def getRandomAction(self, observation):
      '''
      Generiert einen zufälligen Wert zwischen 0-(Anzahl der Reihen * Anzahl der Spalten) aus dem von dem Modell mitgeteilten Optionen für Aktionen, die die Möglichkeiten für den Portalkran darstellen sollen
      '''
      possible_actions_to_choose_from = self.getActionRandC(observation).tolist()
      if len(possible_actions_to_choose_from) != 0:
         action = random.choice(possible_actions_to_choose_from)
         return action
      else:
         return 1
   
   def convertObservation(self, observation_str):
      '''
      Konvertiert die Beobachtung von string -> JSON 
      '''
      observation = json.loads(observation_str)
      return observation

   def getReward(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned das Key-Value Pair des Rewards => Value muss über Index 0 angesprochen werden
      '''
      return observation['Reward']

   def getState(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned das Key-Value Pair der Lagerbestandsliste => Bereitstellungsdatum-Liste kann durch getState_Provision geholt werden
      '''
      return observation['Stellplätze']

   def getActionSpace(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned das Key-Value Pair der möglich ausführbaren Aktionen => hat derzeit keine Verwendung
      '''
      actionspace = observation['Action Space']
      actions = actionspace['Actions']
      return actions

   def getActionRandC(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste mit übersetzten Werten für 'legale' Zeilen- und Spalten-Werte, um diese beim Entscheiden auf den Output zu mappen 
      '''
      action_space = self.getActionSpace(observation)
      rows = [int(value[0]) for key, value in action_space.items()]
      columns = [int(value[1]) for key, value in action_space.items()]
      translatedList = []
      for iter in range(0,len(rows)):
         translatedList.append(self.translateActionBack(rows[iter], columns[iter]))
      print(f"Amount of Options: {len(translatedList)}")
      print(f"OPTIONS {translatedList}")
      translatedTensor = torch.tensor(translatedList).to(self.device)
      
      return translatedTensor

   def getState_Provision(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des ersten Values (Bereitstellungsdatum) das Key-Value Pairs der Lagerbestandsliste
      '''
      states = self.getState(observation)
      state_provision = [int(value[0]) for key, value in states.items()]

      return state_provision

   def getState_ID(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des ersten Values (ID) das Key-Value Pairs der Lagerbestandsliste, derzeit nicht anwendbar
      '''
      states = self.getState(observation)
      state_ID = [value[0] for key, value in states.items()]
      return state_ID

   def getState_Ship(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des dritten Values (Reederei) das Key-Value Pairs der Lagerbestandsliste
      '''
      states = self.getState(observation)
      state_ship = [int(value[2]) for key, value in states.items()]
      return state_ship

   def getState_Dest(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des zweiten Values (Reederei) das Key-Value Pairs der Lagerbestandsliste
      '''
      states = self.getState(observation)
      state_dest = [int(value[1]) for key, value in states.items()]
      return state_dest

   def getState_Weight(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des vierten Values (Gewicht) das Key-Value Pairs der Lagerbestandsliste
      '''
      states = self.getState(observation)
      state_dest = [float(value[3]) for key, value in states.items()]
      return state_dest

   def getState_Pick_Up(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned den Pick Up Place der Beobachtung.
      Der erste Listeneintrag des states-JSON enthält einen Wert mehr, welchen den Abholhort kennzeichnet
      '''
      states = self.getState(observation)
      state_pick_up = states["000"]
      pick_up = state_pick_up[4]
      return pick_up


   def getState_Position(self, observation):
      '''
      Returned eine Liste mit den unforamtierten Positionen des Lagers 
      '''
      states = self.getState(observation)
      state_position = [key for key, value in states.items()]
      return state_position
   
   def list_out_of_pos(self, pos, IDs):
      '''
      Returned eine Liste mit Positionen im Lager, die nicht vakant sind
      '''
      positions = []
      for val in pos:
         if pos.index(val) != 0:
            if len(val) == 4 and int(IDs[pos.index(val)]) != 0:
               positions.append([int(val[:2]), int(val[2:3]), int(val[3:4])])
            elif len(val) == 3 and int(IDs[pos.index(val)]) != 0:
               positions.append([int(val[:1]), int(val[1:2]), int(val[2:3])])
      return positions
            

   def getBatched_State(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine 3x75-Numpy-Array
      '''
      state_pick_up = self.getState_Pick_Up(observation)
      state_p = self.getState_Provision(observation)
      state_s = self.getState_Ship(observation)
      state_d = self.getState_Dest(observation)
      state_w = self.getState_Weight(observation)
      state_list = []
      print(f"PICK UP = {state_pick_up}")
      state_list.append(state_pick_up)
      state_list.extend(state_d)
      state_list.extend(state_s)
      state_list.extend(state_p)
      state_list.extend(state_w)
      
      state_array = np.array(state_list, dtype=int, copy=False)
      print(f"Dimension(I): {len(state_array)}")
      return state_array
   
   def getIsDone(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned den Value das Key-Value Pairs des 'done'-Flags, welches kennzeichnet, ob die Simulationszeit erreicht wurde.
      '''
      return observation["done"]

   def get_confirmation(self):
      '''
      Returned ein String-formatiertes JSON, die den Header für die 'Confirmation' Message enthält
      '''
      confirmation =  {"Header": "Confirmation"}
      confirmation_str = json.dumps(confirmation)
      return str(confirmation_str)

   def get_exception(self):
      '''
      Returned ein String-formatiertes JSON, die den Header für die 'Exception' Message enthält, wenn  beim Agenten etwas schief geht
      Das Modell stoppt die Simulationszeit bei Erhalt dieser Nachricht 
      '''
      stop ={"Header": "Exception"}
      stop_str = json.dumps(stop)
      return str(stop_str)

   def take_action(self, epsilon, device):
      '''
      Methode, welche mit der Wahscheinlichkeit Epsilon entweder die höchste legale Action als Output des DQN im Format (Reihe, Spalte) oder eine zufällige legale Aktion returned
      Der 'Legal-Check' für die zufällige Aktion findet in getRandomAction statt
      '''
      if np.random.random() < epsilon:
         print("RANDOM ACTION")
         actionValue = int(self.getRandomAction(self.last_observation)) 
         action = self.translateAction(actionValue)
      else:
         print("CALCULATED ACTION")
         state_a = self.getBatched_State(self.last_observation)
         state_v = torch.tensor(state_a).to(device)
         print(f"Shape state_v : {state_v.shape}")
        
         q_vals_v = self.net(state_v.float())
         legal_options = self.getActionRandC(self.last_observation)
         actionValue = get_best_legal_action(q_vals_v, legal_options)
         action = self.translateAction(actionValue)
      self.last_action = actionValue
      return action

   

def calc_loss(batch, net, tgt_net, device):
   '''
   Verlustberechnung zwischen States-Zielnetz (Bellmann) und States-Netz
   '''
   states, actions, rewards, dones, next_states = batch
   states_v = torch.tensor(states).to(device)
   next_states_v = torch.tensor(next_states).to(device)
   actions_v = torch.tensor(actions, dtype= torch.int64).to(device)
   rewards_v = torch.tensor(rewards).to(device)
   done_mask = torch.ByteTensor(dones).to(device)
   state_action_values = net(states_v.float()).gather(1, actions_v.unsqueeze(-1)).squeeze(-1)
   next_state_values = tgt_net(next_states_v.float()).max(1)[0]
   next_state_values[done_mask] = 0.0
   next_state_values = next_state_values.detach()
   expected_state_action_values = next_state_values * GAMMA + rewards_v
   loss = nn.MSELoss()(state_action_values, expected_state_action_values)
   print(colours.redify("LOSS: "), loss)
   return loss

def get_best_legal_action(output, legal_output):
   '''
   Mapped den output gegen die noch ausführbaren möglichen Optionen für Actions, die wir vom Modell bekommen und returned aus der Schnittmenge den Index des Outout Neurons mit dem höchsten Wert
   '''
   hierarched, indices = torch.sort(output, descending=True)
   highest_legal_option = None
   if len(legal_output) != 0:
      for val in indices:
          if val in legal_output:
              highest_legal_option = val
              return highest_legal_option.item()
   else:
      #Falls das lager voll ist (nur exception-handling, da das Modell eigentlich nicht mehr senden sollte, falls dies der Fall ist!)
      print(colours.yellowfy("[EVENT]"), " random Action chosen, bacause full...")
      return np.random.randint(0, ACTION_ROWS*ACTION_COLUMNS)



