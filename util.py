
import time
import datetime
import os
import sys
import shutil
import socket
from tkinter import *
import matplotlib.pyplot as plt
import mpl_toolkits
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from PIL import Image
import numpy as np

class colours:
    def __init__(self):
        self.Green = '\033[32m'
        self.White = '\033[37m'
        self.Red = '\033[31m'
        self.Yellow = '\033[33m'
        self.Blue = '\033[34m'
        self.Cyan = '\033[36m'
        self.Purple = '\033[35m'

    def greenify(self, text_to_greenify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine grüne Darstellung in der CLI ermöglichen
        '''
        return self.Green + text_to_greenify + self.White     

    def redify(self, text_to_redify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine rote Darstellung in der CLI ermöglichen
        '''
        return self.Red + text_to_redify + self.White  
    
    def purplify(self, text_to_purplify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine lila Darstellung in der CLI ermöglichen
        '''
        return self.Purple + text_to_purplify + self.White  

    def yellowfy(self, text_to_yellowfy):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine gelbe Darstellung in der CLI ermöglichen
        '''
        return self.Yellow + text_to_yellowfy + self.White  

    def cyanify(self, text_to_cyanify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine hellblaue Darstellung in der CLI ermöglichen
        '''
        return self.Cyan + text_to_cyanify + self.White


class fileDate:
    def __init__(self):
        self.today_time = time.localtime()
        self.today_date = datetime.datetime.now()

    def getIntegerWithZero(self, number):
        '''
        Erhält ein Integer und returned, falls die Zahl kleiner 10 ist eine 0 (z. B. 9 -> "09").
        Notwendig für das Format in welchem die Tensorboard Logfiles gespeichert werden
        '''
        sec_with_zero = f"0{str(number)}" if number <=9 else str(number)
        return sec_with_zero

    def getNow(self):
        '''
        Returned einen String im Format, welcher für das Löschen des Tensorboard Logfiles notwendig ist
        '''
        return(f"{self.today_date.strftime('%b')}{self.getIntegerWithZero(self.today_time.tm_mday)}_{self.getIntegerWithZero(self.today_time.tm_hour)}-{self.getIntegerWithZero(self.today_time.tm_min)}-{self.getIntegerWithZero(self.today_date.second)}")

    def getNowSecLess(self, sec):
        '''
        Returned einen String im Format, welcher für das Löschen des Tensorboard Logfiles notwendig ist
        '''
        return(f"{self.today_date.strftime('%b')}{self.getIntegerWithZero(self.today_time.tm_mday)}_{self.getIntegerWithZero(self.today_time.tm_hour)}-{self.getIntegerWithZero(self.today_time.tm_min)}-{self.getIntegerWithZero(self.today_date.second-sec)}")
        
    
class logWindow:
    '''
    Sollte eine Dashboard-GUI für die Experimente darstellen 
    ''' 
    def __init__(self):
        self.root = Tk()
    
    def add_text(self, textToBeAdded): 
        label = Label(self.root, text=textToBeAdded)

class fileManager:
    def __init__(self):
        self.filedate = fileDate()

    def deleteFolder(self, folder_name):
        '''
        Löscht den angeben Ordner inklusiver aller Inhalte, also Vorsicht.
        Wenn der Ordner nicht gefunden wird, wird eine Exception geworfen
        '''
        try:
            shutil.rmtree(folder_name)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))

    def getLogFolderName(self, GAMMA, LR):
        '''
        Returned den richtigen Logfolder-Namen, falls die Methode zu Beginn der Trainingschleife ausgeführt wird. 
        '''
        folderName = f"runs/{self.filedate.getNow()}_{socket.gethostname()} GAMMA={GAMMA} LR={LR}"
        return folderName
    
    def getLogFolderName_Inference(self, MODEL_PATH):
        '''
        Returned den richtigen Logfolder-Namen, falls die Methode zu Beginn der Trainingschleife ausgeführt wird. 
        '''
        folderName = f"runs/{self.filedate.getNow()}_{socket.gethostname()} Inference_with_savedmodels"
        return folderName
    
    def getLogFolderName_SecLess(self, GAMMA, LR, sec):
        '''
        Returned den richtigen Logfolder-Namen, falls die Methode zu Beginn der Trainingschleife ausgeführt wird mit einem Versatz von anzugebenen Sekunden. 
        '''
        folderName = f"runs/{self.filedate.getNowSecLess(sec)}_{socket.gethostname()} GAMMA={GAMMA} LR={LR}"
        return folderName

    def getLogFolderNames_SecLess(self, GAMMA, LR, begin, end):
        '''
        Kreiert eine Liste in einer anzugeben Range mittels getLogFolderName_SecLess
        '''
        folderNames = []
        for sec in range(begin, end):
            folderNames = self.getLogFolderName_SecLess(GAMMA, LR, sec) 
        return folderNames

class container:
    def __init__(self, ship, provision, weight, position):
        self.weight = weight
        self.ship = ship
        self.provision = provision
        self.ship_colour = self.setColour_ship(self.ship)
        self.provision_colour = self.setColour_Provision(self.provision)
        self.position = position
        self.size = (1, 1, 1)
    def setColour_ship(self, ship):
        '''
        Setzt die Farbe in Hinblick auf die Destination bzw. der Reedereizugehörigkeit des Containers
        '''
        if ship == 1: 
            return 'blue' 
        elif ship == 2: 
            return 'magenta'
        elif ship == 3: 
            return 'darkorange'
    def setColour_Provision(self, destination):
        '''
        Setzt die Farbe in Hinblick auf die Destination bzw. der Reedereizugehörigkeit des Containers
        '''
        if destination == 1: 
            return 'lime' 
        elif destination == 2: 
            return 'yellow'
        elif destination == 3: 
            return 'darkorange'
        elif destination == 4: 
            return 'red'  
        elif destination == 5: 
            return 'darkred'

class storage:
    def __init__(self,dimensions):
        self.containers = list()
        self.dimensions = dimensions
    def appendContainer(self, container):
        self.containers.append(container)
    def getPositions(self):
        positions = []
        for container in self.containers:
            positions.append(container.position)
        return positions
    def getProvision(self):
        provistions = []
        for container in self.containers:
            provistions.append(container.provision)
        return provistions
    def getShip(self):
        ships = []
        for container in self.containers:
            ships.append(container.ship)
        return ships
    def getWeight(self): 
        wheights = []
        for container in self.containers:
            wheights.append(container.wheight)
        return wheights   
    def getSize(self):
        sizes = []
        for container in self.containers:
            sizes.append(container.size)
        return sizes
    def getColours_Prov(self):
        colours = []
        for container in self.containers:
            colours.append(container.provision_colour)
        return colours
    def getColours_Ships(self):
        colours = []
        for container in self.containers:
            colours.append(container.ship_colour)
        return colours 
    

    
        
    

class visualizer:
    def __init__(self, COLUMNS, ROWS, LEVELS):
        self.capacity = COLUMNS*ROWS
        self.storage = storage(dimensions=([1, ROWS+1], [1, COLUMNS+1], [1, LEVELS+1]))

    def printOptionsBar (self, options, capacity, prefix = '', suffix = '', decimals = 1, length = 25, fill = '█', printEnd = "\r"):
        """
        Call in a loop to create a terminal progress bar
        @params:
            options     - Required  : empty Stacks
            capacity    - Required  : total amount of stacks
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        availableOptions = capacity - options
        percent = ("{0:." + str(decimals) + "f}").format(100 * (availableOptions / float(capacity)))
        filledLength = int(length * availableOptions // capacity)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}%{suffix}', end = printEnd)
        # Print New Line on Complete
        #if iteration == total: 
        #print()

    def cuboid_data2(self, o, size=(1,1,1)):
        X = [[[0, 1, 0], [0, 0, 0], [1, 0, 0], [1, 1, 0]],
             [[0, 0, 0], [0, 0, 1], [1, 0, 1], [1, 0, 0]],
             [[1, 0, 1], [1, 0, 0], [1, 1, 0], [1, 1, 1]],
             [[0, 0, 1], [0, 0, 0], [0, 1, 0], [0, 1, 1]],
             [[0, 1, 0], [0, 1, 1], [1, 1, 1], [1, 1, 0]],
             [[0, 1, 1], [0, 0, 1], [1, 0, 1], [1, 1, 1]]]
        X = np.array(X).astype(float)
        for i in range(3):
            X[:,:,i] *= size[i]
        X += np.array(o)
        return X
    
    def plotCubeAt2(self, positions,sizes=None,colors=None, **kwargs):
        if not isinstance(colors,(list,np.ndarray)): colors=["C0"]*len(positions)
        if not isinstance(sizes,(list,np.ndarray)): sizes=[(1,1,1)]*len(positions)
        g = []
        for p,s,c in zip(positions,sizes,colors):
            g.append(self.cuboid_data2(p, size=s) )
        return Poly3DCollection(np.concatenate(g),  
                            facecolors=np.repeat(colors,6), **kwargs)

    def convertObservationToStorage(self, positions):
        '''
        Erhält die konvertierten Listen aus dem Beobachtung-JSON und baut daraus eine Storage-Instanz
        '''
        prov_data = self.getState_Provision()
        ship_data = self.getState_Ship()
        weight_data = self.getState_Weight()
       
    def getState(self, observation):
        '''
        Erhält das BeobachtungsJSON und returned das Key-Value Pair der Lagerbestandsliste => Bereitstellungsdatum-Liste kann durch getState_Provision geholt werden
        '''
        return observation['Stellplätze']

    def getState_Provision(self, observation):
        '''
        Erhält das BeobachtungsJSON und returned eine Liste des ersten Values (Bereitstellungsdatum) das Key-Value Pairs der Lagerbestandsliste
        Kopie aus Q-Agent
        '''
        states = self.getState(observation)
        state_provision = [int(value[0]) for key, value in states.items()]
        return state_provision

    def getState_Ship(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des dritten Values (Reederei) das Key-Value Pairs der Lagerbestandsliste
      Kopie aus Q-Agent
      '''
      states = self.getState(observation)
      state_ship = [int(value[2]) for key, value in states.items()]
      return state_ship

    def getState_Weight(self, observation):
      '''
      Erhält das BeobachtungsJSON und returned eine Liste des vierten Values (Gewicht) das Key-Value Pairs der Lagerbestandsliste
      Kopie aus Q-Agent
      '''
      states = self.getState(observation)
      state_dest = [float(value[3]) for key, value in states.items()]
      return state_dest

    def getState_Position(self, observation):
      '''
      Returned eine Liste mit den unforamtierten Positionen des Lagers 
      '''
      states = self.getState(observation)
      state_position = [key for key, value in states.items()]
      return state_position
   
    def list_out_of_pos(self, pos, IDs):
      '''
      Returned eine Liste mit Positionen im Lager, die nicht vakant sind
      '''
      positions = []
      for val in pos:
         #print(f"{pos.index(val)}: {IDs[pos.index(val)]}")
         if pos.index(val) != 0:
            if len(val) == 4 and int(IDs[pos.index(val)]) != 0:
               positions.append([int(val[:2]), int(val[2:3]), int(val[3:4])])
            elif len(val) == 3 and int(IDs[pos.index(val)]) != 0:
               positions.append([int(val[:1]), int(val[1:2]), int(val[2:3])])
      return positions

    def list_out_of_weight(self, weights):
        '''
        Returned eine Liste mit Gewichtattributen der Stellpläze im Lager, die nicht vakant sind
        '''
        weight_list = []
        for val in weights:
            if weights.index(val) != 0:
                if val != 0:
                    weight_list.append(val)
        return weight_list
    

    def list_out_of_ship(self, ships):
        '''
        Returned eine Liste mit Gewichtattributen der Stellpläze im Lager, die nicht vakant sind
        '''
        ship_list = []
        for val in ships:
            if ships.index(val) != 0:
                if val != 0:
                    ship_list.append(val)
        return ship_list

    def list_out_of_prov(self, provisions):
        '''
        Returned eine Liste mit Gewichtattributen der Stellpläze im Lager, die nicht vakant sind
        '''
        provision_list = []
        for val in provisions:
            if provisions.index(val) != 0:
                if val != 0:
                    provision_list.append(val)
        return provision_list

    def read_in_container_values(self, observation): 
        ships = self.getState_Ship(observation)
        weights = self.getState_Weight(observation)
        positions = self.getState_Position(observation)
        provisions = self.getState_Provision(observation)
        s = self.list_out_of_ship(ships)
        w = self.list_out_of_weight(weights)
        pos = self.list_out_of_pos(positions, provisions)
        prov = self.list_out_of_prov(provisions)
        #Create the container list and add to storage
        for i in range(1, len(s)): 
            cont = container(s[i], prov[i], w[i], pos[i])
            self.storage.appendContainer(cont)

    def visualizeStorage(self):
        positions = self.storage.getPositions()
        sizes = self.storage.getSize()
        ships = self.storage.getShip()
        colors_s = self.storage.getColours_Prov()
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_aspect('auto')
        if len(positions) != 0:
            pc = self.plotCubeAt2(positions,sizes,colors=colors_s, edgecolor="k")
            ax.add_collection3d(pc)   
        ax.set_title("Lagerinhalt nach Destination")
        ax.set_xlabel("Reihen")
        ax.set_ylabel("Spalten")
        ax.set_zlabel("Lagen")
        ax.set_xlim(self.storage.dimensions[1])
        ax.set_ylim(self.storage.dimensions[0])
        ax.set_zlim(self.storage.dimensions[2])
        plt.show()




