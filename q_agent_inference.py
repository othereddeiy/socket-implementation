#This will inherit from q_agent and overwrite its Request Method to do inference instead of training
from q_agent import GAMMA, LEARNING_RATE,writer_folder_name, q_agent

import json
from numpy import random
import torch
import collections
import numpy as np
import time
import random
import logging
from torch.nn.modules import loss 
import torch.optim as optim
import torch.nn as nn
from datetime import datetime
#Workspace
from dqn import DQN
from util import colours, fileDate, fileManager, visualizer
from experienceBuffer import ExperienceBuffer
from server import server
import os
#Tensorboard
from torch.utils.tensorboard import SummaryWriter
#In Pfad das trainierte Modell festlegen
PATH = "savedmodels\MODEL_GAMMA_0.65_LR_0.15_Aug25_08-13-08_35.pth"
LEARNING_RATE
#Tensorboard
writer = SummaryWriter(comment=f"INF{PATH}")
#utilities
colours = colours()
fileDate = fileDate()
fileManager = fileManager()
writer_folder_name_inference = fileManager.getLogFolderName_Inference(PATH)
writer_folder_name_Q = writer_folder_name
#RevisionPath
REV_PATH = f"storage_revision\{fileDate.getNow()}"
class q_agent_inference(q_agent):
    
    def __init__(self, PORT, HEADER, FORMAT, device, net_input, net_output, mode, record=False):
      super(q_agent_inference, self).__init__(PORT, HEADER, FORMAT, device, net_input, net_output, mode)
      #Das Modell laden
      self.net.load_state_dict(torch.load(PATH))
      self.net.eval()
      self.record = record
    
    def receiveRequest(self, conn, addr):
      '''
      Überschreiben der ReceiveRequest-Methode, damit das Modell nicht mehr Traniert wird und lediglich Output gibt
      Generell einfach nur die Trainingskomponenten entfernt
      '''
      time_list = []
      total_rewards = []
      total_loss = []
      best_mean_reward = None
      request_counter = 1
      last_time = 0.0
      print(f"{colours.yellowfy('[NEW CONNECTION]')} {addr} connected")
      connected = True
      while connected:
         
         try:

            #Unser Epsilon immer 0 => keine zufälligen Entscheidungen
            if self.mode == "RL": 
               epsilon = 0   
            else: 
               epsilon = 1
            #Pfad für das Speichern der States
            if not os.path.exists(REV_PATH):
               os.makedirs(REV_PATH)

            msg_length = conn.recv(self.HEADER).decode(self.FORMAT)
            if msg_length: 
               msg_length = int(msg_length)
               msg = conn.recv(msg_length).decode(self.FORMAT)


               if msg == self.DISCONNECT_MESSAGE:
                 #Nutzer fragen, ob die Tensorboard Aufzeichung gespeichert werden soll.
                  print(colours.purplify("Save this tensorboard-logfile? [y/n(any key)]"))
                  response_tensorboard = str(input())
                  if response_tensorboard == "y" or response_tensorboard == "Y":
                     print(colours.yellowfy("[EVENT]"), colours.greenify("Tensorboard logfile was saved!"))
                  else:
                     #LogFile
                     fileManager.deleteFolder(writer_folder_name_inference)
                     #LogFile aus Vererbung
                     fileManager.deleteFolder(writer_folder_name_Q)
                     print(colours.yellowfy("[EVENT]"), colours.redify("Tensorboard logfile was not saved!"))
                  writer.close()
                  connected = False
                  print(colours.yellowfy("[EVENT]"), f"[{addr}] : disconnected")

                  
               elif msg[0:6] == self.UPDATE_MESSAGE:
                  self.last_observation = self.convertObservation(msg[6:])


               elif msg == self.REQUEST_MESSAGE:
                  print("------------------------------------------------------------------------------------------------------------------------------------------------")
                  #Zeit messen
                  start_time = time.time()
                  delta_time = start_time-last_time
                  time_list.append(delta_time)
                  last_time = start_time
                  m_time = np.mean(time_list[100:])
                  print(f"{colours.cyanify('REQUEST')} {request_counter}")
                  request_counter += 1
                  actionSuggestion = self.take_action(epsilon, device=self.device)
                  print(f"EPSILON = {epsilon}")
                  print(f"∆: {delta_time}")
                  writer.add_scalar("Performance/Seconds per Request", m_time, global_step=request_counter)
                  writer.add_scalar("Performance/Requests per Second", 1/m_time, global_step=request_counter)
                  conn.send(actionSuggestion.encode(self.FORMAT))


               if msg[0:1] == self.OBSERVATION_MESSAGE:
                  #Beobachtungswerte für einfacheren Zugriff in einfachen Variablen speichern 
                  observation = self.convertObservation(msg)

                  self.last_observation = observation
                  if self.record == True:
                     f = open(f"{REV_PATH}/req{request_counter}.json", "x")
                     f.write(msg)
                     f.close()
                  new_state = self.getBatched_State(observation)
                  reward = self.getReward(observation)
                  actions = self.getActionSpace(observation)
                  done = self.getIsDone(observation)
                  self.total_Reward += reward

                  #Letzten State vorspeichern 
                  self.last_state = new_state
                  #Rewards speichern
                  total_rewards.append(reward)

                  #Mittleren Reward der letzten 100 Steps speichern
                  m_reward = np.mean(total_rewards[100:])

                  if done:
                     #writer.close()
                     #torch.save(self.net.state_dict(), PATH)
                     print(f"{colours.yellowfy('[EVENT]')} {colours.greenify(' Simulation beendet')}")

                  #Plotting
                  writer.add_scalar("Epsilon", epsilon, global_step=request_counter)
                  writer.add_scalar("Reward/Noisy Reward", reward, global_step=request_counter)
                  writer.add_scalar("Reward/Total Reward", self.total_Reward, global_step=request_counter)
                  writer.add_scalar("Reward/Mean Reward (Last 100 steps)", m_reward, global_step=request_counter)

                  #Debugging
                  #print(f"action: {actions}")
                  print(f"REWARD: {reward}")
                  print(f"DONE: {done}")
                  conn.send(self.get_confirmation().encode(self.FORMAT))
         except Exception as e:
            logging.exception('')
            conn.send(self.get_exception().encode(self.FORMAT))
            print(colours.redify("Please reset the simualation to save both the logfile and model!"))
            #print(colours.yellowfy("[EVENT]"), colours.redify(f"Exception: {e}"))

      #Training beendet
      print(colours.greenify("[EVENT] TRAINING ENDED"))
      #Verbindung zum Client beenden
      conn.close()
      return False
        


    