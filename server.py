import socket
import json 
import socket
import concurrent.futures

from torch.utils.tensorboard.summary import text
from util import colours
colours = colours()
class server:
    '''
    Server-Klasse, die als Parent, des Agenten fungiert und grundlegende Kommunikation mit dem Client des SImulationsmodell ermöglicht
    '''
    def __init__(self, PORT, HEADER, FORMAT):
        self.IP = socket.gethostbyname(socket.gethostname())
        self.PORT = PORT 
        self.HEADER = HEADER
        self.FORMAT = FORMAT
        self.DISCONNECT_MESSAGE = "DISCONNECT"
        self.ADDR = (self.IP, self.PORT)
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(self.ADDR)
        self.client_list = {}
        self.exec = concurrent.futures.ThreadPoolExecutor()
        

    def start(self):
        '''
        Starten des Servers, nutzt eine Threading API (Futures) und eröffnet einen Thread, falls sich ein Client mit uns verbindet
        Im Thread selber wird die ReceiveRequest Methode des Agenten submitted, wenn diese den Value 'False' returned, schließt der Thread 
        '''
        client_counter = 1
        self.server.listen()
        print(colours.greenify("[LISTENING]"), "Agent is listening on", self.IP)
        not_quit = True
        while not_quit:
           conn, addr = self.server.accept()
           self.client_list[str(client_counter)] = conn
           client_counter += 1
           with self.exec as executor:
               future = executor.submit(self.receiveRequest, conn, addr)
               return_value = future.result()
               if return_value == False:
                   not_quit = False
    
            
    def send(self,conn, message):
        conn.send(message.encode(self.FORMAT))

    def close(self):
        self.server.close()
        print ("closed")    